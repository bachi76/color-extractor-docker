# color-extractor-docker

A docker setup for https://github.com/algolia/color-extractor

Updated to Python 3.7 and latest OpenCV / Scikit etc. Some other requirements were updated to solve issues with Python 3.7.

Usage example: 
```$ docker run -it bachi/color-extractor ./color-extractor color_names.npz https:/.../some.jpg```

Further docs at https://github.com/algolia/color-extractor
